const fs = require('fs')
const mariadb = require('mariadb')

const connection = mariadb.createConnection({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'password',
    database: 'tech_test'
})

connection.then(async conn => {
    for (const query of fs.readFileSync('db/schema.sql').toString().split(';')) {
        if (query.trim() !== '') {
            try {
                await conn.query(query)
            } catch (error) {
                console.error(error)
                process.exit(1)
            }
        }
    }

    async function createGameSession(name) {
        let insertedId

        try {
            await conn.query(
                `INSERT INTO game_session (session_name) VALUES ('${name}')`
            ).then(result => {
                insertedId = result.insertId
            })
        } catch (error) {
            console.error(error)
            process.exit(1)
        }

        return insertedId
    }

    async function createPlayer(firstName, lastName, contactNumber, sessionId) {
        try {
            await conn.query(
                `INSERT INTO player (
                    first_name,
                    last_name,
                    contact_number,
                    game_session_id
                    ) VALUES (
                        '${firstName}',
                        '${lastName}',
                        '${contactNumber}',
                        '${sessionId}'
                        )`
            )
        } catch (error) {
            console.error(error)
            process.exit(1)
        }
    }

    const sessionOneId = await createGameSession('Black Rain')
    const sessionTwoId = await createGameSession('One Last Riddle')
    const sessionThreeId = await createGameSession('The Burning Plague')
    const sessionFourId = await createGameSession('The Sea Witch')
    const sessionFiveId = await createGameSession('Tomb Of Horrors')

    await createPlayer('Joe', 'Caputo', '07658312387', sessionOneId)
    await createPlayer('Piper', 'Chapman', '07142548798', sessionOneId)
    await createPlayer('Tasha', 'Jefferson', '07998987220', sessionOneId)
    await createPlayer('Gloria', 'Mendoza', '07512645873', sessionOneId)
    await createPlayer('Theodore', 'Bagwell', '07561384896', sessionTwoId)
    await createPlayer('Brad', 'Bellick', '07883256418', sessionTwoId)
    await createPlayer('Lincoln', 'Burrows', '07112356983', sessionTwoId)
    await createPlayer('Fernando', 'Sucre', '07963212321', sessionTwoId)
    await createPlayer('Sara', 'Tancredi', '07954186684', sessionTwoId)
    await createPlayer('Daryl', 'Dixon', '07325649845', sessionThreeId)
    await createPlayer('Maggie', 'Greene', '07459832185', sessionThreeId)
    await createPlayer('Carol', 'Peletier', '07124979566', sessionThreeId)
    await createPlayer('Eugene', 'Porter', '07223654987', sessionThreeId)
    await createPlayer('Billy', 'Cranston', '07985645784', sessionFourId)
    await createPlayer('Kimberly', 'Hart', '07815307459', sessionFourId)
    await createPlayer('Trini', 'Kwan', '07548755285', sessionFourId)
    await createPlayer('Tommy', 'Oliver', '07989444568', sessionFourId)
    await createPlayer('Jason', 'Scott', '07774854987', sessionFourId)
    await createPlayer('Zack', 'Taylor', '07845222547', sessionFourId)
    await createPlayer('Joyce', 'Byers', '07954668187', sessionFiveId)
    await createPlayer('Dustin', 'Henderson', '07889554857', sessionFiveId)
    await createPlayer('Jim', 'Hopper', '07954845148', sessionFiveId)
    await createPlayer('Nancy', 'Wheeler', '07445845711', sessionFiveId)

    console.log('Completed')
})