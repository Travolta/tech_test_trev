const express = require('express')
const mariadb = require('mariadb')

//create database connectipn
const config = {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'password',
    database: 'tech_test'
}

const app = express()
const port = 3000

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.use(express.json())

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

//Define post request for sending single queries
app.post('/query', (req, res) => {
    mariadb.createConnection(config)
        .then(conn => {
            conn.query(req.body.body)
                .then(result => {
                    res.send(result)
                    conn.end()
                }).catch(error => {
                    conn.end()

                    throw new Error(error)
                })
        }).catch(error => {

            throw new Error(error)
        })
})

//Define post request for sending multiple queries in a transaction
app.post('/transactionQueries', (req, res) => {
    mariadb.createConnection(config)
        .then(conn => {
            conn.beginTransaction()
                .then(() => {
                    for (const query of req.body.body) {

                        return conn.query(query)
                    }
                })
                .then(
                    function success() {
                        conn.commit()
                        res.send()
                        conn.end()
                    },
                    function fail(reason) {
                        conn.rollback()
                        res.send()
                        conn.end()
                    }
                )
        }).catch(error => {
            
            throw new Error(error)
        })
})