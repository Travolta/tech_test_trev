import axios from 'axios'
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { InputWrapper, TableFilterWrapper } from '../components/wrappers'
import { Link, useHistory, useParams } from 'react-router-dom'
import { TableCell, TableHeadCell, TableRow } from '../components/basics'
import { editSession, deleteSession } from '../configuration/session'
import { faAngleDoubleLeft, faAngleDoubleRight, faArrowLeft, faChevronLeft, faChevronRight, faPen, faTrash } from '@fortawesome/free-solid-svg-icons'
import { useTableAssembler } from '../hooks'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface SessionDetailsProps {
    sessions: {
        id: number,
        sessionName?: string
    }[]
}

/**
 * Player table filters
 */
const idFilter = new TableFilterWrapper()
const forenameFilter = new TableFilterWrapper()
const surnameFilter = new TableFilterWrapper()

const filters = [idFilter, forenameFilter, surnameFilter]

export const SessionDetails: FunctionComponent<SessionDetailsProps> = props => {
    const { sessionId } = useParams()
    const history = useHistory()
    const [deleteDialog, setDeleteDialog] = useState([])
    const [inputs, setInputs] = useState<InputWrapper[]>([])
    const [actions, setActions] = useState<JSX.Element[]>([])
    const [editing, setEditing] = useState(false)
    const [session, setSession] = useState(
        {
            id: 0,
            sessionName: ''
        }
    )

    const [fetchedPlayers, setPlayers] = useState([
        {
            id: 0,
            firstName: '',
            lastName: '',
            gameSession: ''
        }
    ])

    let players = fetchedPlayers
    const tableAssembler = useTableAssembler()

    const currentSessionPosition = props.sessions.findIndex(object => object.id === session.id)
    const previousSessionId = props.sessions[currentSessionPosition - 1] ? props.sessions[currentSessionPosition - 1].id : session.id
    const nextSessionId = props.sessions[currentSessionPosition + 1] ? props.sessions[currentSessionPosition + 1].id : session.id
    const firstSessionId = props.sessions[0].id
    const lastSessionId = props.sessions[props.sessions.length - 1].id

    const filterValues = () => {
        const fieldsToCheck: string[] = []

        if (idFilter.getValue()) {
            fieldsToCheck.push('id')
        }

        if (forenameFilter.getValue()) {
            fieldsToCheck.push('forename')
        }

        if (surnameFilter.getValue()) {
            fieldsToCheck.push('surname')
        }

        if (fieldsToCheck.length > 0) {
            players = fetchedPlayers.filter(player => {
                const validArray = []

                for (const field of fieldsToCheck) {
                    switch (field) {
                        case 'id': validArray.push(String(player.id).includes(idFilter.getValue()))
                            break
                        case 'forename': validArray.push(player.firstName.includes(forenameFilter.getValue()))
                            break
                        case 'surname': validArray.push(player.lastName.includes(surnameFilter.getValue()))
                            break
                    }
                }

                if (!validArray.includes(false)) {
                    return player
                }

                return false
            })
        } else {
            players = fetchedPlayers
        }

        const newTableBody = buildTableBody()
        tableAssembler.setTableBody([newTableBody])
    }

    idFilter.setFilterFunction(filterValues)
    forenameFilter.setFilterFunction(filterValues)
    surnameFilter.setFilterFunction(filterValues)

    const buildTableHead = useCallback(() => (
        <TableRow>
            <TableHeadCell>ID</TableHeadCell>
            <TableHeadCell>Forename</TableHeadCell>
            <TableHeadCell>Surname</TableHeadCell>
        </TableRow>
    ), [])

    const buildTableFilterRow = useCallback(() => (
        <TableRow>
            <TableCell>{idFilter.build()}</TableCell>
            <TableCell>{forenameFilter.build()}</TableCell>
            <TableCell>{surnameFilter.build()}</TableCell>
        </TableRow>
    ), [])

    const buildTableBody = useCallback(() => (
        players.sort((first, second) => second.id - first.id).map(player => (
            <TableRow key={player.id}>
                <TableCell>
                    <Link
                        className='table__row__cell__link'
                        to={`/players/${player.id}`}
                    >
                        {player.id}
                    </Link>
                </TableCell>
                <TableCell>{player.firstName} </TableCell>
                <TableCell>{player.lastName}</TableCell>
            </TableRow>
        ))
    ), [players])

    const buildTableFooter = useCallback(() => (
        <TableRow>
            <TableCell />
            <TableCell />
            <TableCell />
            <TableCell>{`Total players: ${players.length}`}</TableCell>
        </TableRow>
    ), [players])

    //build table
    useEffect(() => {
        tableAssembler.setTableHead([buildTableHead()])
        tableAssembler.setTableFilterRow([buildTableFilterRow()])
        tableAssembler.setTableBody([buildTableBody()])
        tableAssembler.setTableFooter([buildTableFooter()])
    }, [buildTableHead, buildTableFilterRow, buildTableBody, buildTableFooter])


    const handleDeleteClick = useCallback(() => {
        const sessionDeletionData = {
            id: session.id,
            sessionName: session.sessionName,
            playerCount: fetchedPlayers.length
        }
        
        deleteSession(setDeleteDialog, sessionDeletionData, history)
    }, [session, fetchedPlayers, history])
    const handleEditClick = useCallback(() => editSession(setInputs, setActions, setEditing, tableAssembler, filters, fetchedPlayers, session), [session, fetchedPlayers])

    // Switch edit and delete actions
    useEffect(() => {
        if (!editing) {
            setActions([
                <button
                    key='edit'
                    className='card__header__button'
                    onClick={() => handleEditClick()}
                >
                    <FontAwesomeIcon icon={faPen} />
                </button>,
                <button
                    key='delete'
                    className='card__header__button'
                    onClick={() => handleDeleteClick()}
                >
                    <FontAwesomeIcon icon={faTrash} />
                </button>
            ])
        }
    }, [handleEditClick, handleDeleteClick, editing])

    //fetch session details and players
    useEffect(() => {
        const source = axios.CancelToken.source()
        let subscribed = true

        function getSession() {
            if (subscribed) {
                const query = `
                SELECT s.id, s.session_name AS sessionName
                FROM game_session AS s
                WHERE s.id = '${sessionId}'
                `

                instance.post('/query', { body: query }).then(result => setSession(result.data[0]))
            }
        }

        getSession()

        function getSessionPlayers() {
            if (subscribed) {
                const query = `
                SELECT p.id, p.first_name AS firstName, p.last_name AS lastName
                FROM player AS p
                WHERE p.game_session_id = '${sessionId}'
                `

                instance.post('/query', { body: query }).then(result => setPlayers(result.data))
            }
        }

        getSessionPlayers()

        return () => {
            subscribed = false
            source.cancel()
        }
    }, [sessionId])

    const parseAvatarText = (): string => {
        const wordsArray = session.sessionName.split(' ')
        let string = ''

        for (const word of wordsArray) {
            string = string + `${word.charAt(0).toUpperCase()}`
        }

        return string
    }

    return (
        <>
            <>
                {deleteDialog}
            </>
            <div className='card--sessions'>
                <div className='card__header header--sessions'>
                    <Link to='/sessions'>
                        <button className='card__header__button'>
                            <FontAwesomeIcon icon={faArrowLeft} />
                        </button>
                    </Link>
                    <span className='card__header__text'>
                        {`${session.sessionName}`}
                    </span>
                    <div className='card__header__actionbutton_container'>
                        {actions}
                    </div>
                </div>
                <div className='card__grid_container'>
                    <div className='card__grid_container__avatar_container'>
                        <span className='card__grid_container__avatar_container__avatar'>
                            {parseAvatarText()}
                        </span>
                        {editing && inputs.map(InputWrapper => InputWrapper.build())}
                    </div>
                    <div className='card__grid_container__field_container'>
                        {tableAssembler.buildTable()}
                    </div>
                </div>
                <div className='card__footer footer--sessions' >
                    <div className='card__footer__navbutton_container'>
                        <Link to={`/sessions/${firstSessionId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faAngleDoubleLeft} />
                            </button>
                        </Link>
                        <Link to={`/sessions/${previousSessionId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faChevronLeft} />
                            </button>
                        </Link>
                        <div className='card__footer__navbutton_container__text'>
                            {currentSessionPosition + 1} of {props.sessions.length}
                        </div>
                        <Link to={`/sessions/${nextSessionId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faChevronRight} />
                            </button>
                        </Link>
                        <Link to={`/sessions/${lastSessionId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faAngleDoubleRight} />
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}
