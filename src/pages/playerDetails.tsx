import axios from 'axios'
import React, { FunctionComponent, useCallback } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { InputWrapper } from '../components/wrappers'
import { Link, useHistory, useParams } from 'react-router-dom'
import { deletePlayer, editPlayer } from '../configuration/player'
import { faAngleDoubleLeft, faAngleDoubleRight, faArrowLeft, faChevronLeft, faChevronRight, faPen, faTrash } from '@fortawesome/free-solid-svg-icons'
import { useEffect, useState } from 'react'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface PlayerDetailsProps {
    players: {
        id: number,
        firstName?: string,
        lastName?: string,
        gameSession?: string
    }[]
}

export const PlayerDetails: FunctionComponent<PlayerDetailsProps> = props => {
    const { playerId } = useParams()
    const history = useHistory()
    const [deleteDialog, setDeleteDialog] = useState([])
    const [player, setPlayer] = useState(
        {
            id: 0,
            firstName: '',
            lastName: '',
            gameSession: '',
            gameSessionId: 0,
            contactNumber: ''
        }
    )

    useEffect(() => {
        const source = axios.CancelToken.source()
        let subscribed = true

        function getPlayer() {
            if (subscribed) {
                const query = `
                SELECT p.id, p.first_name AS firstName, p.last_name AS lastName, p.contact_number AS contactNumber, gs.session_name as gameSession, gs.id as gameSessionId
                FROM player AS p
                LEFT JOIN game_session AS gs ON gs.id = p.game_session_id
                WHERE p.id = '${playerId}'
                `

                instance.post('/query', { body: query }).then(result => setPlayer(result.data[0]))
            }
        }

        getPlayer()

        return () => {
            subscribed = false
            source.cancel()
        }
    }, [playerId])

    const currentPlayerPosition = props.players.findIndex(object => object.id === player.id)
    const previousPlayerId = props.players[currentPlayerPosition - 1] ? props.players[currentPlayerPosition - 1].id : player.id
    const nextPlayerId = props.players[currentPlayerPosition + 1] ? props.players[currentPlayerPosition + 1].id : player.id
    const firstPlayerId = props.players[0].id
    const lastPlayerId = props.players[props.players.length - 1].id

    const [inputs, setInputs] = useState<InputWrapper[]>([])

    useEffect(() => {
        const sessionInput = new InputWrapper('Game session', player.gameSession, true)
        const contactNumberInput = new InputWrapper('Contact number', player.contactNumber, true)
        setInputs([sessionInput, contactNumberInput])
    }, [player])

    const handleDeleteClick = useCallback(() => deletePlayer(setDeleteDialog, player, history), [history, player])
    const handleEditClick = useCallback(() => editPlayer(setInputs, setActions, setEditing, player), [player])

    const [actions, setActions] = useState<JSX.Element[]>([])
    const [editing, setEditing] = useState(false)

    useEffect(() => {
        if (!editing) {
            setActions([
                <button
                    key='edit'
                    className='card__header__button'
                    onClick={() => handleEditClick()}
                >
                    <FontAwesomeIcon icon={faPen} />
                </button>,
                <button
                    key='delete'
                    className='card__header__button'
                    onClick={() => handleDeleteClick}
                >
                    <FontAwesomeIcon icon={faTrash} />
                </button>
            ])
        }
    }, [handleEditClick, handleDeleteClick, editing])

    return (
        <>
            <>
                {deleteDialog}
            </>
            <div className='card--players'>
                <div className='card__header header--players'>
                    <Link to='/players'>
                        <button className='card__header__button'>
                            <FontAwesomeIcon icon={faArrowLeft} />
                        </button>
                    </Link>
                    <span className='card__header__text'>
                        {`${player.firstName} ${player.lastName}`}
                    </span>
                    <div className='card__header__actionbutton_container'>
                        {actions}
                    </div>
                </div>
                <div className='card__grid_container'>
                    <div className='card__grid_container__avatar_container'>
                        <span className='card__grid_container__avatar_container__avatar'>
                            {`${player.firstName.charAt(0).toUpperCase()}${player.lastName.charAt(0).toUpperCase()}`}
                        </span>
                    </div>
                    <div className='card__grid_container__field_container'>
                        {inputs.map(InputWrapper => InputWrapper.build())}
                    </div>
                </div>
                <div className='card__footer footer--players' >
                    <div className='card__footer__navbutton_container'>
                        <Link to={`/players/${firstPlayerId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faAngleDoubleLeft} />
                            </button>
                        </Link>
                        <Link to={`/players/${previousPlayerId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faChevronLeft} />
                            </button>
                        </Link>
                        <div className='card__footer__navbutton_container__text'>
                            {currentPlayerPosition + 1} of {props.players.length}
                        </div>
                        <Link to={`/players/${nextPlayerId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faChevronRight} />
                            </button>
                        </Link>
                        <Link to={`/players/${lastPlayerId}`}>
                            <button className='card__footer__navbutton_container__button'>
                                <FontAwesomeIcon icon={faAngleDoubleRight} />
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}
