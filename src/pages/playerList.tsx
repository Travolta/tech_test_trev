import React, { FunctionComponent, useEffect, useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { TableCell, TableHeadCell, TableRow } from '../components/basics'
import { TableFilterWrapper } from '../components/wrappers'
import { createPlayer } from '../configuration/player'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { fetchPlayers } from '../configuration/player'
import { useTableAssembler } from '../hooks'

const idFilter = new TableFilterWrapper()
const forenameFilter = new TableFilterWrapper()
const surnameFilter = new TableFilterWrapper()
const sessionFilter = new TableFilterWrapper()

export const PlayerList: FunctionComponent = () => {
    const [createDialog, setCreateDialog] = useState([])
    const [fetchedPlayers, setPlayers] = useState([
        {
            id: 0,
            firstName: '',
            lastName: '',
            gameSession: ''
        }
    ])

    const tableAssembler = useTableAssembler()
    let players = fetchedPlayers

    const filterValues = () => {
        const fieldsToCheck: string[] = []

        if (idFilter.getValue()) {
            fieldsToCheck.push('id')
        }

        if (forenameFilter.getValue()) {
            fieldsToCheck.push('forename')
        }

        if (surnameFilter.getValue()) {
            fieldsToCheck.push('surname')
        }

        if (sessionFilter.getValue()) {
            fieldsToCheck.push('session')
        }

        if (fieldsToCheck.length > 0) {
            players = fetchedPlayers.filter(player => {
                const validArray = []

                for (const field of fieldsToCheck) {
                    switch (field) {
                        case 'id': validArray.push(String(player.id).includes(idFilter.getValue()))
                            break
                        case 'forename': validArray.push(player.firstName.includes(forenameFilter.getValue()))
                            break
                        case 'surname': validArray.push(player.lastName.includes(surnameFilter.getValue()))
                            break
                        case 'session': validArray.push(player.gameSession.includes(sessionFilter.getValue()))
                            break
                    }
                }

                if (!validArray.includes(false)) {
                    return player
                }

                return false
            })
        } else {
            players = fetchedPlayers
        }

        tableAssembler.setTableBody([buildTableBody()])
        tableAssembler.setTableFooter([buildTableFooter()])
    }

    idFilter.setFilterFunction(filterValues)
    forenameFilter.setFilterFunction(filterValues)
    surnameFilter.setFilterFunction(filterValues)
    sessionFilter.setFilterFunction(filterValues)

    const buildTableHead = useCallback(() => (
        <TableRow>
            <TableHeadCell>ID</TableHeadCell>
            <TableHeadCell>Forename</TableHeadCell>
            <TableHeadCell>Surname</TableHeadCell>
            <TableHeadCell>Session</TableHeadCell>
        </TableRow>
    ), [])

    const buildTableFilterRow = useCallback(() => (
        <TableRow>
            <TableCell>{idFilter.build()}</TableCell>
            <TableCell>{forenameFilter.build()}</TableCell>
            <TableCell>{surnameFilter.build()}</TableCell>
            <TableCell>{sessionFilter.build()}</TableCell>
        </TableRow>
    ), [])

    const buildTableBody = useCallback(() => (
        players.sort((first, second) => second.id - first.id).map(player => (
            <TableRow key={player.id}>
                <TableCell>
                    <Link
                        className='table__row__cell__link'
                        to={`/players/${player.id}`}
                    >
                        {player.id}
                    </Link>
                </TableCell>
                <TableCell>{player.firstName} </TableCell>
                <TableCell>{player.lastName}</TableCell>
                <TableCell>{player.gameSession}</TableCell>
            </TableRow>
        ))
    ), [players])

    const buildTableFooter = useCallback(() => (
        <TableRow>
            <TableCell />
            <TableCell />
            <TableCell />
            <TableCell>{`Players: ${players.length}`}</TableCell>
        </TableRow>
    ), [players])

    //buildTable
    useEffect(() => {
        tableAssembler.setTableHead([buildTableHead()])
        tableAssembler.setTableFilterRow([buildTableFilterRow()])
        tableAssembler.setTableBody([buildTableBody()])
        tableAssembler.setTableFooter([buildTableFooter()])
    }, [buildTableHead, buildTableFilterRow, buildTableBody, buildTableFooter])

    //fetch players
    useEffect(() => fetchPlayers(setPlayers), [])

    const handleClickCreatePlayer = () => createPlayer(setCreateDialog)

    return (
        <>
            <>
                {createDialog}
            </>
            <div className='card--players'>
                <div className='card__header header--players'>
                    <span className='card__header__text'>
                        Players
                            </span>
                    <div className='card__header__actionbutton_container'>
                        <button
                            className='card__header__button'
                            onClick={() => handleClickCreatePlayer()}
                        >
                            <FontAwesomeIcon icon={faPlus} />
                        </button>
                    </div>
                </div>
                {tableAssembler.buildTable()}
                <div className='card__footer footer--players' />
            </div>
        </>
    )
}