import React, { FunctionComponent, useEffect, useState, useCallback } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'
import { TableCell, TableHeadCell, TableRow } from '../components/basics'
import { TableFilterWrapper } from '../components/wrappers'
import { createSession } from '../configuration/session'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { fetchSessions } from '../configuration/session'
import { useTableAssembler } from '../hooks'

const idFilter = new TableFilterWrapper()
const sessionFilter = new TableFilterWrapper()

export const SessionList: FunctionComponent = () => {
    const [createDialog, setCreateDialog] = useState([])
    const [fetchedSessions, setSessions] = useState([
        {
            id: 0,
            sessionName: ''
        }
    ])

    const tableAssembler = useTableAssembler()
    let sessions = fetchedSessions

    const filterValues = () => {
        const fieldsToCheck: string[] = []

        if (idFilter.getValue()) {
            fieldsToCheck.push('id')
        }

        if (sessionFilter.getValue()) {
            fieldsToCheck.push('session')
        }

        if (fieldsToCheck.length > 0) {
            sessions = fetchedSessions.filter(session => {
                const validArray = []

                for (const field of fieldsToCheck) {
                    switch (field) {
                        case 'id': validArray.push(String(session.id).includes(idFilter.getValue()))
                            break
                        case 'session': validArray.push(session.sessionName.includes(sessionFilter.getValue()))
                            break
                    }
                }

                if (!validArray.includes(false)) {
                    return session
                }

                return false
            })
        } else {
            sessions = fetchedSessions
        }

        tableAssembler.setTableBody([buildTableBody()])
        tableAssembler.setTableFooter([buildTableFooter()])
    }

    idFilter.setFilterFunction(filterValues)
    sessionFilter.setFilterFunction(filterValues)

    const buildTableHead = useCallback(() => (
        <TableRow>
            <TableHeadCell>ID</TableHeadCell>
            <TableHeadCell>Name</TableHeadCell>
        </TableRow>
    ), [])

    const buildTableFilterRow = useCallback(() => (
        <TableRow>
            <TableCell>{idFilter.build()}</TableCell>
            <TableCell>{sessionFilter.build()}</TableCell>
        </TableRow>
    ), [])

    const buildTableBody = useCallback(() => (
        sessions.sort((first, second) => second.id - first.id).map(session => (
            <TableRow key={session.id}>
                <TableCell>
                    <Link
                        className='table__row__cell__link'
                        to={`/sessions/${session.id}`}
                    >
                        {session.id}
                    </Link>
                </TableCell>
                <TableCell>{session.sessionName} </TableCell>
            </TableRow>
        )
        )
    ), [sessions])

    const buildTableFooter = useCallback(() => (
            <TableRow>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{`Sessions: ${sessions.length}`}</TableCell>
            </TableRow>
        ), [sessions])

    const lastSessionId = sessions.sort((first, second) => second.id - first.id)[0].id
    const handleClickCreateSession = () => createSession(setCreateDialog, lastSessionId)

    //buildTable
    useEffect(() => {
        tableAssembler.setTableHead([buildTableHead()])
        tableAssembler.setTableFilterRow([buildTableFilterRow()])
        tableAssembler.setTableBody([buildTableBody()])
        tableAssembler.setTableFooter([buildTableFooter()])
    }, [buildTableHead, buildTableFilterRow, buildTableBody, buildTableFooter])

    //fetch all sessions
    useEffect(() => fetchSessions(setSessions), [])

    return (
        <>
            <>
                {createDialog}
            </>
            <div className='card--sessions'>
                <div className='card__header header--sessions'>
                    <span className='card__header__text'>
                        Sessions
                    </span>
                    <div className='card__header__actionbutton_container'>
                        <button
                            className='card__header__button'
                            onClick={() => handleClickCreateSession()}
                        >
                            <FontAwesomeIcon icon={faPlus} />
                        </button>
                    </div>
                </div>
                {tableAssembler.buildTable()}
                <div className='card__footer footer--sessions' />
            </div>
        </>
    )
}