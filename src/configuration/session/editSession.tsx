import axios from 'axios'
import React from 'react'
import { CheckboxWrapper, InputWrapper, TableFilterWrapper } from '../../components/wrappers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { SessionPropertyInputPair, validateSession } from './validators'
import { TableAssembler } from '../../hooks'
import { TableCell, TableHeadCell, TableRow } from '../../components/basics'
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'


const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface Session {
    id: number,
    sessionName: string
}

interface Player {
    id: number,
    firstName: string,
    lastName: string
}

export async function editSession(setInputs: Function, setActions: Function, setEditing: Function, tableAssembler: TableAssembler, filters: TableFilterWrapper[], players: Player[], session: Session): Promise<void> {
    const checkboxes: { playerId: number, checkbox: CheckboxWrapper }[] = []
    const sessionNameField = new InputWrapper("Session name", session.sessionName)
    const newArray: (InputWrapper)[] = [sessionNameField]

    setInputs(newArray)
    setEditing(true)
    setActions([
        <button
            className='card__header__button'
            onClick={() => handleCancelClick()}
        >
            <FontAwesomeIcon icon={faTimes} />
        </button>,
        <button
            className='card__header__button'
            onClick={() => handleSaveClick()}
        >
            <FontAwesomeIcon icon={faCheck} />
        </button>
    ])

    tableAssembler.setTableHead([
        <TableRow>
            <TableHeadCell>ID</TableHeadCell>
            <TableHeadCell>Forename</TableHeadCell>
            <TableHeadCell>Surname</TableHeadCell>
            <TableHeadCell>Remove?</TableHeadCell>
        </TableRow>
    ])

    tableAssembler.setTableFilterRow([
        <TableRow>
            {filters.map(filter => (
                <TableCell>{filter.build()}</TableCell>
            ))}
            <TableCell />
        </TableRow>
    ])

    tableAssembler.setTableBody([
        players.sort((first, second) => second.id - first.id).map(player => {
            const checkbox = new CheckboxWrapper()
            checkboxes.push({ playerId: player.id, checkbox })

            return (
                <TableRow key={player.id}>
                    <TableCell>{player.id}</TableCell>
                    <TableCell>{player.firstName} </TableCell>
                    <TableCell>{player.lastName}</TableCell>
                    <TableCell>{checkbox.build()}</TableCell>
                </TableRow>
            )
        })
    ])

    const handleCancelClick = () => {
        setEditing(false)
        window.location.reload()
    }

    const handleSaveClick = () => {
        const validationPairs: SessionPropertyInputPair[] = [{ property: 'session_name', input: sessionNameField }]

        if (validateSession(validationPairs)) {
            const playersToRemove = checkboxes.filter(object => object.checkbox.getChecked() === true).map(object => object.playerId)
            const queries = []

            if (sessionNameField.getValue() !== session.sessionName) {
                const query = `
                    UPDATE game_session
                    SET session_name = "${sessionNameField.getValue()}"
                    WHERE id = ${session.id};
                    `
                queries.push(query)
            }

            if (playersToRemove.length > 0) {
                const query = `
                    UPDATE player
                    SET game_session_id = NULL
                    WHERE id IN (${playersToRemove.join()});
                    `
                queries.push(query)
            }

            if (queries.length > 0) {
                instance.post('/transactionQueries', { body: queries }).then(() => window.location.reload())
            }
        } else {
            setInputs([...newArray])
        }
    }
}