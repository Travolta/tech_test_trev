import { InputWrapper } from "../../../components/wrappers"

export interface SessionPropertyInputPair {
    property: 'session_name'
    input: InputWrapper
}

export function validateSession(pairs: SessionPropertyInputPair[]): boolean {
    let pass = true

    for (const pair of pairs) {
        switch (pair.property) {
            case 'session_name': {
                const value = pair.input.getValue()

                if (value && value.length <= 20) {
                    pair.input.clearError()
                } else if (!value) {
                    pass = false
                    pair.input.setError('Name should not be empty')
                } else if (value.length > 20) {
                    pass = false
                    pair.input.setError('Max length: 20')
                }

                break
            }
        }
    }

    return pass
}