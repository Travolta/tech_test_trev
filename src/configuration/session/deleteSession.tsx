import axios from 'axios'
import React from 'react'
import { Dialog } from '../../components/basics'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface Session {
    id: number,
    sessionName: string,
    playerCount: number
}

export async function deleteSession(setDeleteDialog: Function, session: Session, history: any): Promise<void> {
    const handleClose = () => setDeleteDialog([])

    const handleDeleteClick = () => {
        if (session.playerCount > 0) {
            setDeleteDialog([])
            return
        }

        const query = `
        DELETE FROM game_session
        WHERE id = ${session.id}
        `

        instance.post('/query', { body: query }).then(() => {
            setDeleteDialog([])
            history.push('/sessions')
            window.location.reload()
        })
    }

    const deleteButton = <button
        className='dialog__action_container__button'
        onClick={() => handleDeleteClick()}
    >
        {session.playerCount === 0 ? 'DELETE' : 'CLOSE'}
    </button>

const dialogText = session.playerCount === 0
    ? `Are you sure you wish to delete ${session.sessionName}`
    : 'Please remove all players from this session before deletion'

    const dialog = (
        <Dialog
            title='Delete session'
            actions={[deleteButton]}
            handleClose={handleClose}
        >
            {dialogText}
        </Dialog>
    )

    setDeleteDialog([dialog])
}