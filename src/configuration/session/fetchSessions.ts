import axios from 'axios'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

export function fetchSessions(setSessions: Function) {
    const source = axios.CancelToken.source()
    let subscribed = true
    
    function getSessions() {
        if (subscribed) {
            const query = `
            SELECT s.id, s.session_name AS sessionName
            FROM game_session AS s
            `
            
            instance.post('/query', { body: query }).then(result => setSessions(result.data))
        }
    }
    
    getSessions()
    
    return () => {
        subscribed = false
        source.cancel()
    }
}