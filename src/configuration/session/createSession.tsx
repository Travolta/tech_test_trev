import axios from 'axios'
import React from 'react'
import { Dialog } from '../../components/basics'
import { InputWrapper } from '../../components/wrappers'
import { SessionPropertyInputPair, validateSession } from './validators'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

export async function createSession(setCreateDialog: Function, lastSessionId: number): Promise<void> {
    const handleClose = () => setCreateDialog([])

    const sessionId = new InputWrapper('ID').setValue(`${lastSessionId + 1}`).setReadOnly(true)
    const nameInput = new InputWrapper("Session name")

    const handleCreateClick = () => {
        const validationPairs: SessionPropertyInputPair[] = [{ property: 'session_name', input: nameInput }]

        if (validateSession(validationPairs)) {
            const query = `
                INSERT INTO game_session (
                    session_name
                )
                VALUES ("${nameInput.getValue()}")
                `

            instance.post('/query', { body: query }).then(() => {
                setCreateDialog([])
                window.location.reload()
            })
        } else {
            setCreateDialog(buildDialog())
        }
    }

    const createButton = <button
        className='dialog__action_container__button'
        onClick={() => handleCreateClick()}
    >
        CREATE
    </button>

    const inputs = [sessionId, nameInput]

    const buildDialog = () => (
        <Dialog
            title='Create session'
            actions={[createButton]}
            handleClose={handleClose}
        >
            {inputs.map(input => input.build())}
        </Dialog>
    )

    setCreateDialog([buildDialog()])
}