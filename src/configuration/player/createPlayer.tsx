import axios from 'axios'
import React from 'react'
import { Dialog } from '../../components/basics'
import { InputWrapper, SelectWrapper } from '../../components/wrappers'
import { PlayerPropertyInputPair, validatePlayer } from './validators'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

export async function createPlayer(setCreateDialog: Function): Promise<void> {
    const handleClose = () => setCreateDialog([])

    const firstNameInput = new InputWrapper("First name")
    const lastNameInput = new InputWrapper("Last name")
    const contactNumberInput = new InputWrapper("Contact number")

    const handleCreateClick = () => {
        const validationPairs: PlayerPropertyInputPair[] = [
            { property: 'first_name', input: firstNameInput },
            { property: 'last_name', input: lastNameInput },
            { property: 'contact_number', input: contactNumberInput }
        ]

        if (validatePlayer(validationPairs)) {
            const query = `
                    INSERT INTO player (
                        game_session_id,
                        first_name,
                        last_name,
                        contact_number
                    )
                    VALUES (
                        ${gameSessionSelect.getValue().id ? `"${gameSessionSelect.getValue().id}"` : null},
                        "${firstNameInput.getValue()}",
                        "${lastNameInput.getValue()}",
                        "${contactNumberInput.getValue()}"
                    )
                    `

            instance.post('/query', { body: query }).then(() => {
                setCreateDialog([])
                window.location.reload()
            })
        } else {
            setCreateDialog([buildDialog()])
        }
    }

    const createButton = <button
        className='dialog__action_container__button'
        onClick={() => handleCreateClick()}
    >
        CREATE
    </button>

    const sessions: { id: string, value: string }[] = []

    const getSessions = async () => {
        const query = `
                SELECT gs.id AS id, gs.session_name AS sessionName
                FROM game_session AS gs
                `

        instance.post('/query', { body: query }).then(result => {
            result.data.forEach((session: { id: string, sessionName: string }) => {
                sessions.push({
                    id: session.id,
                    value: session.sessionName
                })
            })
        })
    }

    await getSessions()

    const gameSessionSelect = new SelectWrapper('Game session', sessions)

    const inputs = [
        firstNameInput,
        lastNameInput,
        contactNumberInput,
        gameSessionSelect
    ]

    const buildDialog = () => (
        <Dialog
            title='Create player'
            actions={[createButton]}
            handleClose={handleClose}
        >
            {inputs.map(input => input.build())}
        </Dialog>
    )

    setCreateDialog([buildDialog()])
}