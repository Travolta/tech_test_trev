import axios from 'axios'
import React from 'react'
import { Dialog } from '../../components/basics'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface Player {
    id: number,
    firstName?: string,
    lastName?: string,
    gameSession?: string
}

export async function deletePlayer(setDeleteDialog: Function, player: Player, history: any): Promise<void> {
    const handleClose = () => setDeleteDialog([])

    const handleDeleteClick = () => {
        const query = `
                DELETE FROM player
                WHERE id = ${player.id}
                `

        instance.post('/query', { body: query }).then(() => {
            setDeleteDialog([])
            history.push("/players")
            window.location.reload()
        })
    }

    const deleteButton = <button
        className='dialog__action_container__button'
        onClick={() => handleDeleteClick()}
    >
        DELETE
    </button>

    const dialog = (
        <Dialog
            title='Delete player'
            actions={[deleteButton]}
            handleClose={handleClose}
        >
        {`Are you sure you wish to remove ${player.firstName} ${player.lastName}?`}
        </Dialog>
    )

    setDeleteDialog([dialog])
}