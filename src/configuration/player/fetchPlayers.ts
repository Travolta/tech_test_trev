import axios from 'axios'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

export function fetchPlayers(setPlayers: Function) {
    const source = axios.CancelToken.source()
    let subscribed = true
    
    function getPlayers() {
        if (subscribed) {
            const query = `
            SELECT p.id, p.first_name AS firstName, p.last_name AS lastName, gs.session_name as gameSession
            FROM player AS p
            LEFT JOIN game_session AS gs ON gs.id = p.game_session_id
            `
            
            instance.post('/query', { body: query }).then(result => setPlayers(result.data))
        }
    }
    
    getPlayers()
    
    return () => {
        subscribed = false
        source.cancel()
    }
}