import { InputWrapper } from "../../../components/wrappers"

export interface PlayerPropertyInputPair {
    property: 'first_name' | 'last_name' | 'contact_number'
    input: InputWrapper
}

export function validatePlayer(pairs: PlayerPropertyInputPair[]): boolean {
    let pass = true

    for (const pair of pairs) {
        switch (pair.property) {
            case 'first_name': {
                const value = pair.input.getValue()

                if (value && value.length <= 20) {

                    pair.input.clearError()
                } else if (!value) {
                    pass = false
                    pair.input.setError('Name should not be empty')
                } else if (value.length > 20) {
                    pass = false
                    pair.input.setError('Max length: 20')
                }

                break
            }

            case 'last_name': {
                const value = pair.input.getValue()

                if (value && value.length <= 20) {

                    pair.input.clearError()
                } else if (!value) {
                    pass = false
                    pair.input.setError('Name should not be empty')
                } else if (value.length > 20) {
                    pass = false
                    pair.input.setError('Max length: 20')
                }

                break
            }
            
            case 'contact_number': {
                const value = pair.input.getValue()

                if (value && value.length <= 11) {

                    pair.input.clearError()
                } else if (!value) {
                    pass = false
                    pair.input.setError('Contact number should not be empty')
                } else if (value.length > 11) {
                    pass = false
                    pair.input.setError('Max length: 11')
                }
            }
        }
    }

    return pass
}