import axios from 'axios'
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { InputWrapper, SelectWrapper } from '../../components/wrappers'
import { PlayerPropertyInputPair, validatePlayer } from './validators'
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'

const instance = axios.create({ baseURL: 'http://localhost:3000' })

interface Player {
    id: number,
    firstName?: string,
    lastName?: string,
    gameSession?: string,
    gameSessionId?: number,
    contactNumber?: string
}

export async function editPlayer(setInputs: Function, setActions: Function, setEditing: Function, player: Player): Promise<void> {
    setEditing(true)

    setActions([
        <button
            className='card__header__button'
            onClick={() => handleCancelClick()}
        >
            <FontAwesomeIcon icon={faTimes} />
        </button>,
        <button
            className='card__header__button'
            onClick={() => handleSaveClick()}
        >
            <FontAwesomeIcon icon={faCheck} />
        </button>
    ])

    const sessions: { id: string, value: string }[] = []

    const getSessions = async () => {
        const query = `
                SELECT gs.id AS id, gs.session_name AS sessionName
                FROM game_session AS gs
                `

        instance.post('/query', { body: query }).then(result => {
            result.data.forEach((session: { id: string, sessionName: string }) => {
                sessions.push({
                    id: session.id,
                    value: session.sessionName
                })
            })
        })
    }

    await getSessions()
    const playerSession = player.gameSessionId && player.gameSession
        ? { id: String(player.gameSessionId), value: player.gameSession }
        : undefined

    const firstNameField = new InputWrapper("First name", player.firstName)
    const lastNameField = new InputWrapper("Last name", player.lastName)
    const gameSessionField = new SelectWrapper("Game session", sessions, playerSession)
    const contactNumberField = new InputWrapper('Contact number', player.contactNumber)

    const newArray: (InputWrapper | SelectWrapper)[] = [firstNameField, lastNameField, gameSessionField, contactNumberField]
    setInputs(newArray)

    const handleCancelClick = () => {
        setEditing(false)
        window.location.reload()
    }
    const handleSaveClick = () => {
        const validationPairs: PlayerPropertyInputPair[] = [
            {property: 'first_name', input: firstNameField},
            {property: 'last_name', input: lastNameField},
            {property: 'contact_number', input: contactNumberField}
        ]

        if (validatePlayer(validationPairs)) {
            const query = `
                    UPDATE player
                    SET first_name = "${firstNameField.getValue()}",
                        last_name = "${lastNameField.getValue()}",
                        contact_number = "${contactNumberField.getValue()}",
                        game_session_id = ${gameSessionField.getValue().id ? Number(gameSessionField.getValue().id) : null}
                    WHERE id = ${player.id}
                    `
    
            instance.post('/query', { body: query }).then(() => {
                window.location.reload()
            })
        } else {
            setInputs([...newArray])
        }
    }
}