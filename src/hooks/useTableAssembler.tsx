import React from 'react'
import { Table } from '../components/basics'
import { useState } from 'react'

export interface TableAssembler {
        buildTable: () => JSX.Element
        setTableHead: React.Dispatch<React.SetStateAction<JSX.Element[]>>
        setTableFilterRow: React.Dispatch<React.SetStateAction<JSX.Element[]>>
        setTableBody: React.Dispatch<React.SetStateAction<JSX.Element[][]>>
        setTableFooter: React.Dispatch<React.SetStateAction<JSX.Element[]>>
}

/**
 * Hook based function for easy assembly and updating of tables
 */
export function useTableAssembler(): TableAssembler {
    const [tableHead, setTableHead] = useState<JSX.Element[]>([])
    const [tableFilterRow, setTableFilterRow] = useState<JSX.Element[]>([])
    const [tableBody, setTableBody] = useState<JSX.Element[][]>([])
    const [tableFooter, setTableFooter] = useState<JSX.Element[]>([])

    const buildTable = () => (
        <Table
            head={tableHead[0]}
            filterRow={tableFilterRow[0]}
            body={tableBody[0]}
            footer={tableFooter[0]}
        />
    )

    return {
        buildTable,
        setTableHead,
        setTableFilterRow,
        setTableBody,
        setTableFooter
    }
}
