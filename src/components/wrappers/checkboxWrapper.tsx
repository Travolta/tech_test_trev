import React from 'react'
import { Checkbox } from '../basics'

export class CheckboxWrapper {
    protected checked: boolean = false

    constructor() {
        this.setChecked = this.setChecked.bind(this)
    }

    public setChecked() {
        this.checked = !this.checked

        return this
    }

    public getChecked() {
        return this.checked
    }

    public build() {
        return (
            <Checkbox 
                checkedHandler={this.setChecked}
                checked={this.checked}
            />
        )
    }
}