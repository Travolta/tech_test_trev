import React from 'react'
import { Select, SelectOption } from '../basics'

export class SelectWrapper {
    protected value: {id: string, value:string}

    constructor(protected label: string, protected options: SelectOption[], value?: SelectOption) {
        this.value = value ? value : {id: "", value: ""}
    }

    public setValue(value: {id: string, value:string}) {
        this.value = value

        return this
    }

    public getValue() {

        return this.value
    }

    public build() {

        return (
            <Select
                label={this.label}
                options={this.options}
                value={this.value}
                valueHandler={this.setValue.bind(this)}
            />
        )
    }
}