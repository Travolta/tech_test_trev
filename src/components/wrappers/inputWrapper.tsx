import React from 'react'
import { Input } from '../basics'

export class InputWrapper {
    protected value: string
    protected readonly: boolean
    protected error: string = ''

    constructor(protected label: string, value?: string, readonly?: boolean) {
        this.value = value ? value : ""
        this.readonly = readonly ? readonly : false

        this.setReadOnly = this.setReadOnly.bind(this)
        this.setValue = this.setValue.bind(this)
    }

    public setReadOnly(value: boolean) {
        this.readonly = value

        return this
    }

    public setValue(value: string) {
        this.value = value

        return this
    }

    public getValue() {

        return this.value
    }

    public setError(error: string) {
        this.error = error

        return this
    }

    public clearError() {
        this.error = ''

        return this
    }

    public build() {

        return (
            <Input
                key={this.label}
                label={this.label}
                value={this.value}
                valueHandler={this.setValue}
                readonly={this.readonly}
                error={this.error}
            />
        )
    }
}