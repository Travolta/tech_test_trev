import React from 'react'
import { TableFilter } from '../basics'

export class TableFilterWrapper {
    protected value: string = ""

    constructor(protected filterFunction?: Function) {

        this.setValue = this.setValue.bind(this)
    }

    public setValue(value: string) {
        this.value = value

        return this
    }

    public setFilterFunction(filterFunction: Function) {
        this.filterFunction = filterFunction

        return this
    }

    public getValue() {
        return this.value
    }

    public build() {

        return (
            <TableFilter
                filterFunction={this.filterFunction}
                value={this.value}
                valueHandler={this.setValue}
            />
        )
    }
}