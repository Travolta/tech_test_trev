import React, { FunctionComponent } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

interface DialogProps {
    title: string
    actions: JSX.Element[]
    handleClose: Function
}

export const Dialog: FunctionComponent<DialogProps> = props => {
    const handleCloseClick = () => props.handleClose() 

    return (
        <>
            <div className='dialog_background' />
            <div className='dialog'>
                <div className='dialog__title_container'>
                    <span className='dialog__title_container__title'>
                        {props.title}
                    </span>
                    <button 
                        className='dialog__title_container__closebutton'
                        onClick={() => handleCloseClick()}
                        >
                        <FontAwesomeIcon icon={faTimes} />
                    </button>
                </div>
                <div className='dialog__content_container'>
                    {props.children}
                </div>
                <div className='dialog__action_container'>
                    {props.actions}
                </div>
            </div>
        </>
    )
}