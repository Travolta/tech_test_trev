import React, { FunctionComponent, useState, useEffect } from 'react'

interface InputProps {
    label?: string
    value?: string
    readonly?: boolean
    valueHandler: Function,
    error: string
}

export const Input: FunctionComponent<InputProps> = props => {
    const [focused, setFocused] = useState(false)
    const [error, setError] = useState('')

    const handleClick = () => setFocused(true)
    const handleBlur = () => setFocused(false)
    const handleChange = (value: string) => props.valueHandler(value)

    useEffect(() => setError(props.error), [props.error])

    return (
        <div className='input_container'>
            <span className={focused ? 'input_container__label--focus' : 'input_container__label'}>{props.label}</span>
            <input
                className='input_container__input'
                type='text'
                defaultValue={props.value}
                readOnly={props.readonly}
                onClick={() => handleClick()}
                onBlur={() => handleBlur()}
                onChange={(event) => handleChange(event.currentTarget.value)}
                autoComplete="none"
            />
            {error.length > 0 && <div className={'input_container__error_message'}>{error}</div>}
        </div>
    )
}