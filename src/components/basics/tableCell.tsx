import React, { FunctionComponent } from 'react'

interface TableCellProps {
}

export const TableCell: FunctionComponent<TableCellProps> = props => {

    return (
        <td className='table__row__cell'>
            {props.children}
        </td>
    )
}