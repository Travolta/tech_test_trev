import React, { FunctionComponent } from 'react'

interface TableHeadProps {

}

export const TableHead: FunctionComponent<TableHeadProps> = props => {

    return (
        <thead className='table__head'>
            {props.children}
        </thead>
    )
}