import React, { FunctionComponent, useState } from 'react'

export interface SelectOption { id: string, value: string }

interface SelectProps {
    label?: string
    value?: SelectOption
    readonly?: boolean
    options: SelectOption[]
    valueHandler: Function
}

export const Select: FunctionComponent<SelectProps> = props => {
    const [focused, setFocused] = useState(false)
    const [selected, setSelected] = useState({ id: '', value: '' })

    let optionClick = false

    const handleClick = () => setFocused(true)
    const handleBlur = () => !optionClick && setFocused(false)

    const buildOptions = () => {
        const builtOptions: JSX.Element[] = []

        const buildNoneOption = () => {
            const handleOptionClick = () => {
                const object = { id: '', value: 'None' }
                setSelected(object)
                setFocused(false)
                optionClick = false
                props.valueHandler(object)
            }

            return (
                <div
                    onMouseDown={() => optionClick = true}
                    onClick={() => handleOptionClick()}
                    className='select_option'
                    key={'0'}
                >
                    {'None'}
                </div>
            )
        }

        builtOptions.push(
            buildNoneOption(),
            ...props.options.map(object => {
                const handleOptionClick = () => {
                    setSelected(object)
                    setFocused(false)
                    optionClick = false
                    props.valueHandler(object)
                }

                return (
                    <div
                        onMouseDown={() => optionClick = true}
                        onClick={() => handleOptionClick()}
                        className='select_option'
                        key={object.id}
                    >
                        {object.value}
                    </div>
                )
            }))

        return builtOptions
    }

    return (
        <div className='input_container'>
            <span className={focused ? 'input_container__label--focus' : 'input_container__label'}>{props.label}</span>
            <input
                className='input_container__input'
                type='text'
                defaultValue={selected.value.length > 0 ? selected.value : props.value ? props.value.value: ''}
                value={selected.value.length > 0 ? selected.value : props.value ? props.value.value: ''}
                readOnly={props.readonly}
                onClick={() => handleClick()}
                onBlur={() => handleBlur()}
            />
            {focused && !props.readonly && (
                <div className='input_container__select_options_popover'>
                    <div className='card--sessions'>
                        {buildOptions()}
                    </div>
                </div>
            )}
        </div>
    )
}