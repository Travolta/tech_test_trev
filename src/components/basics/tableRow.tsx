import React, { FunctionComponent } from 'react'

interface TableRowProps {

}

export const TableRow: FunctionComponent<TableRowProps> = props => {

    return (
        <tr className='table__row'>
            {props.children}
        </tr>
    )
}