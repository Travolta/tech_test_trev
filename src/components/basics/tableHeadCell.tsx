import React, { FunctionComponent } from 'react'

interface TableHeadCellProps {
}

export const TableHeadCell: FunctionComponent<TableHeadCellProps> = props => {

    return (
        <th className='table__row__head_cell'>
            {props.children}
        </th>
    )
}