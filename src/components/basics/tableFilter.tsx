import React, { FunctionComponent } from 'react'

interface TableFilterProps {
    value?: string
    readonly?: boolean
    valueHandler: Function
    filterFunction?: Function
}

export const TableFilter: FunctionComponent<TableFilterProps> = props => {
    const handleChange = (value: string) => {
        props.valueHandler(value)

        if (props.filterFunction) {
            props.filterFunction()
        }
    }

    return (
        <div className='table_filter_container'>
            <input
                className='table_filter_container__input'
                type='text'
                defaultValue={props.value}
                readOnly={props.readonly}
                onChange={(event) => handleChange(event.currentTarget.value)}
                autoComplete="none"
            />
        </div>
    )
}