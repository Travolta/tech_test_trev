import React, { FunctionComponent } from 'react'
import { TableBody } from './tableBody'
import { TableFooter } from './tableFooter'
import { TableHead } from './tableHead'

interface TableProps {
    head?: JSX.Element
    filterRow?: JSX.Element
    body?: JSX.Element[]
    footer?: JSX.Element
}

export const Table: FunctionComponent<TableProps> = props => {
    return (
        <table className='table'>
            <TableHead>
                {props.head}
                {props.filterRow}
            </TableHead>
            <TableBody>
                {props.body}
            </TableBody>
            <TableFooter>
                {props.footer}
            </TableFooter>
        </table>
    )
}