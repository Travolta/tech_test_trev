import React, { FunctionComponent, useState } from 'react'

interface CheckboxProps {
    checked?: boolean
    checkedHandler: Function
}

export const Checkbox: FunctionComponent<CheckboxProps> = props => {
    const [checked, setChecked] = useState(false)

    const handleChange = () => {
        props.checkedHandler()
        setChecked(!checked)
    }

    return (
        <div className='checkbox_container'>
            <label className={checked ? 'checkbox_container__label--checked' : 'checkbox_container__label'}>
            <input
                className='checkbox_container__checkbox'
                type='checkbox'
                defaultChecked={props.checked}
                onChange={(event) => handleChange()}
            />
            </label>
        </div>
    )
}