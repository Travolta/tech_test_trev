import React, { FunctionComponent } from 'react'

interface TableFooterProps {
}

export const TableFooter: FunctionComponent<TableFooterProps> = props => {

    return (
        <tfoot className='table__footer'>
            {props.children}
        </tfoot>
    )
}