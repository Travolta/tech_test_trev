import React, { FunctionComponent } from 'react'

interface TableBodyProps {

}

export const TableBody: FunctionComponent<TableBodyProps> = props => {

    return (
        <tbody className='table__body'>
            {props.children}
        </tbody>
    )
}