import './App.css'

import React from 'react'

import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { PlayerDetails, PlayerList, SessionDetails, SessionList } from './pages'
import { faDice, faUserFriends } from '@fortawesome/free-solid-svg-icons'
import { fetchPlayers } from './configuration/player'
import { fetchSessions } from './configuration/session'
import { useEffect, useState } from 'react'

function AppRouter() {
    const [players, setPlayers] = useState([
        {
            id: 0,
            firstName: '',
            lastName: '',
            gameSession: ''
        }
    ])

    const [sessions, setSessions] = useState([
        {
            id: 0,
            sessionName: ''
        }
    ])

    useEffect(() => fetchPlayers(setPlayers), [])
    useEffect(() => fetchSessions(setSessions), [])

    return (
        <Router>
            <div className='titlebar'>Dice and Dungeons</div>
            <div className='application'>
                <div className='navbar'>
                    <Link to='/players'>
                        <button className='navbar__button button--players'>
                            <FontAwesomeIcon icon={faUserFriends} />
                        </button>
                    </Link>
                    <Link to='/sessions'>
                        <button className='navbar__button button--sessions'>
                            <FontAwesomeIcon icon={faDice} />
                        </button>
                    </Link>
                </div>

                <div className='router_content' >
                    <Switch >
                        <Route path='/players/:playerId'>
                            <PlayerDetails players={players}/>
                        </Route>
                        <Route path='/players'>
                            <PlayerList />
                        </Route>
                        <Route path='/sessions/:sessionId'>
                            <SessionDetails sessions={sessions}/>
                        </Route>
                        <Route path='/sessions'>
                            <SessionList />
                        </Route>
                    </Switch>
                </div>
            </div>
        </Router>
    )
}

export default AppRouter
