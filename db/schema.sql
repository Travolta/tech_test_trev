DROP DATABASE IF EXISTS tech_test;
CREATE DATABASE tech_test;
USE tech_test;

CREATE TABLE game_session (
    PRIMARY KEY (id),

    id                  BIGINT          NOT NULL AUTO_INCREMENT,
    session_name        VARCHAR(20)     NOT NULL
);

CREATE TABLE player (
    PRIMARY KEY (id),

    id                  BIGINT          NOT NULL AUTO_INCREMENT,
    game_session_id     BIGINT          DEFAULT NULL,
    first_name          VARCHAR(20)     NOT NULL,
    last_name           VARCHAR(20)     NOT NULL,
    contact_number      VARCHAR(11)     NOT NULL,

    CONSTRAINT FOREIGN KEY (game_session_id) REFERENCES game_session (id)
);
