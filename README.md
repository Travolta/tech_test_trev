This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It contains all of the usual create-react-app scripts.

The backend is running on an express-server. To run the backend you must use the following scripts:
- npm run generate-schema | This builds the tables and prefills some data
- npm run start-express-server | This runs the server that allows quering of the database

Once these scripts have been ran you can simply run standard react start scripts to launch a dev environment.

As for the project itself, This little tech test follows a vague set of stories from my place of work with the goal of building a simple game session - player tracking system.
You are able to create players and sessions, assign/remove players to sessions, and edit the details of players and sessions.

I deliberatley set myself limitations for this project to only use basic React and raw sql. Additonally I wrote and styled all of the components myself with css
instead of using a frontend library like material-ui.
Since my usual place of work uses a great many packages I intended to use this project to really get to grips with React Hooks 
and everything that you can and cannot do with them.

I had a great time putting it all together and i'm pretty happy with how everything turned out :)

-Matt "Mothra" Saunders